CC = gcc
CFLAGS = -O2 -Wall -Iinclude

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread

all: client server

client: src/echoclient.c csapp.o
	$(CC) $(CFLAGS) -o client src/echoclient.c csapp.o $(LIB)

server: src/echoserveri.c csapp.o
	$(CC) $(CFLAGS) -o server src/echoserveri.c csapp.o $(LIB)

csapp.o: src/csapp.c
	$(CC) $(CFLAGS) -c src/csapp.c

clean:
	rm -f *.o client server *~

